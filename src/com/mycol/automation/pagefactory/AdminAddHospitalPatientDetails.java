package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.Hospital;

public class AdminAddHospitalPatientDetails {
	protected WebDriver driver;
	private By pageheading = By.xpath(".//*[@id='content']/h1");
	private By linkpatientdetails = By
			.linkText("(C) Patient Details");
	private By homefoodallowed = By.id("Hospital_home_food_allowed");
	private By dailyvisitors = By.id("Hospital_daily_vistors");
	private By allowedvisitors = By.id("Hospital_allowed_visitors");
	private By ipddailypaitent = By.id("Hospital_ipd_patients_daily_basis");
	private By ipdinceptionpaitent = By
			.id("Hospital_ipd_patients_since_inception");
	private By opdpaitentdaily = By.id("Hospital_opd_patients_daily_basis");
	private By opdinceptionpaitent = By
			.id("Hospital_opd_patients_since_inception");
	private By avrleangthofstay = By.id("Hospital_alos");
	private By occupancyrate = By.id("Hospital_occupancy_rate");
	private By opdtimingfrm = By.id("Hospital_from_opd_timing_0");
	private By opdtimingto = By.id("Hospital_to_opd_timing_0");
	private By visitinghrsfrm = By.id("Hospital_from_visiting_hours_0");
	private By visitinghrsto = By.id("Hospital_to_visiting_hours_0");
	private By tieuphospital = By.id("Hospital_autosuggest");
	private By btnsumbit = By
			.xpath(".//*[@id='hospital-form-3']/div[19]/input");
	private By successnotification = By
			.xpath(".//*[@id='msg-hospital-form-3']");

	public AdminAddHospitalPatientDetails(WebDriver driver) {
		this.driver = driver;
	}

	public void openPatientDetailPage() {
	
		FrameWork.Click(driver, linkpatientdetails);
	}

	public void doAddPatientDetailHospital() throws InterruptedException {
		Log.info("Adding Patient in  hospital..");
		FrameWork.Type(driver, dailyvisitors,
				Hospital.patientdetails.dailyvisitorsText);
		FrameWork.SelectByVisibleText(driver, homefoodallowed,
				Hospital.patientdetails.homefoodallowedText);
		FrameWork.Type(driver, allowedvisitors,
				Hospital.patientdetails.allowedvisitorsText);
		FrameWork.Type(driver, ipddailypaitent,
				Hospital.patientdetails.ipddailypaitentdailyText);
		FrameWork.Type(driver, ipdinceptionpaitent,
				Hospital.patientdetails.ipdinceptionpaitentsinceText);
		FrameWork.Type(driver, opdpaitentdaily,
				Hospital.patientdetails.opdpaitentdailyText);
		FrameWork.Type(driver, opdinceptionpaitent,
				Hospital.patientdetails.opdinceptionpaitentText);
			FrameWork.Type(driver, avrleangthofstay,
				Hospital.patientdetails.avrleangthofstayText);
		FrameWork.Type(driver,occupancyrate,
				Hospital.patientdetails.occupancyrateText);
		FrameWork.SelectByVisibleText(driver, opdtimingfrm,
				Hospital.patientdetails.opdtimingfrmText);
		FrameWork.SelectByVisibleText(driver, opdtimingto,
				Hospital.patientdetails.opdtimingtoText);
		FrameWork.SelectByVisibleText(driver, visitinghrsfrm,
				Hospital.patientdetails.visitinghrsfrmText);
		FrameWork.SelectByVisibleText(driver, visitinghrsto,
				Hospital.patientdetails.visitinghrstoText);
		FrameWork.Type(driver, tieuphospital,
				Hospital.patientdetails.tieuphospitalText);
		FrameWork.Click(driver, btnsumbit);
		Thread.sleep(5000);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver,successnotification,Hospital.patientdetails.successnotificationText));
		
		


	}

	public void verifyAddedPatientDetailHospital() {

		Log.info("verifying AddedPatientDetailHospital");
		
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, dailyvisitors,
				Hospital.patientdetails.dailyvisitorsText),"daily visitors not matched");

		Assert.assertTrue(FrameWork.verifyDropDownData(driver, homefoodallowed,
				Hospital.patientdetails.homefoodallowedText),"homefood allowed not matched");

		Assert.assertTrue(FrameWork.verifInputFieldData(driver,
				allowedvisitors, Hospital.patientdetails.allowedvisitorsText),"allowed visitors not matched");

		Assert.assertTrue(FrameWork.verifInputFieldData(driver,
				ipddailypaitent, Hospital.patientdetails.ipddailypaitentdailyText),"ipddailypaitentdaily not matched");

		Assert.assertTrue(FrameWork.verifInputFieldData(driver,
				ipdinceptionpaitent,
				Hospital.patientdetails.ipdinceptionpaitentsinceText),"ipdinceptionpaitentsinceText not matched");

		Assert.assertTrue(FrameWork.verifInputFieldData(driver,
				avrleangthofstay, Hospital.patientdetails.avrleangthofstayText),"avrleangthofstayText not matched");
		
		Assert.assertTrue(FrameWork.verifInputFieldData(driver,occupancyrate,
				Hospital.patientdetails.occupancyrateText),"occupancyrate not matched");

		Assert.assertTrue(FrameWork.verifyDropDownData(driver, opdtimingfrm,
				Hospital.patientdetails.opdtimingfrmText),"opdtimingfrmText not matched");

		Assert.assertTrue(FrameWork.verifyDropDownData(driver, opdtimingto,
				Hospital.patientdetails.opdtimingtoText),"opd timing not matched");

		Assert.assertTrue(FrameWork.verifyDropDownData(driver, visitinghrsfrm,
				Hospital.patientdetails.visitinghrsfrmText));

		Assert.assertTrue(FrameWork.verifyDropDownData(driver, visitinghrsto,
				Hospital.patientdetails.visitinghrstoText),"visitinghrstoText not matched ");

		Assert.assertTrue(FrameWork.verifInputFieldData(driver, tieuphospital,
				Hospital.patientdetails.tieuphospitalText),"tieuphospitalText not matched ");

	}


	public void verifyPatientDetailPage() {

		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading,
				Hospital.patientdetails.pageheadingpatientdetailsText),"Patient detail page not correct");

	}
	
	public void clearAll()
	{
		Log.info("Clearinig Patient in  hospital..");
		FrameWork.GetElement(driver, dailyvisitors);
		FrameWork.GetElement(driver, allowedvisitors);
		FrameWork.GetElement(driver, ipddailypaitent);
		FrameWork.GetElement(driver, ipdinceptionpaitent);
		FrameWork.GetElement(driver, opdpaitentdaily);
		FrameWork.GetElement(driver, opdinceptionpaitent);
		FrameWork.GetElement(driver, ipdinceptionpaitent);
		FrameWork.GetElement(driver, avrleangthofstay);
		FrameWork.GetElement(driver, tieuphospital);
	}
	

}
