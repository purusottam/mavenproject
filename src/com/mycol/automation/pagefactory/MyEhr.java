package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.MyEhrData;

public class MyEhr {
	protected WebDriver driver;

	public MyEhr(WebDriver driver) {
		this.driver = driver;
	}
	
	private By welcome = By.xpath("html/body/div[1]/div[1]/nav/ul[2]/li[3]/a");
	private By myehr = By.linkText("My EHR");
	private By pageheading = By.xpath(".//*[@id='content']/div[2]/div/div[1]/div[1]");
	private By mine = By.linkText("Mine");
	private By btnnewehr = By.id("add_new_ehr_button");
	private By uploadfile = By.className("dz-default dz-message");
	private By btnAddTag = By.id("add_ehr_tag");
	private By txtdateehr = By.id("ehrDate");
	private By ehrtype = By.id("ehrType");
	private By txtdoctor = By.id("ehrDoctor");
	private By txthospital = By.id("hospital");
	private By txtdiease = By.id("disease");
	private By txtreporttype = By.id("reportType");
	private By btnsaveehr = By.id("save_ehr");
	private By btncancelehr = By.id("add_ehr_tag");
	private By btnAddNewEHR = By.id("add_new_ehr_button");
	private By btnReports = By.
			xpath("/html/body/div[1]/div[2]/div[2]/div/div/div[3]/div/div/div[3]/div[5]/form/div/div[2]/div[2]/div[2]/div/label[3]");
	private By btnBills = By.
			xpath("/html/body/div[1]/div[2]/div[2]/div/div/div[3]/div/div/div[3]/div[5]/form/div/div[2]/div[2]/div[2]/div/label[2]");
	private By btnOK = By.xpath("html/body/div[5]/div/div/div[2]/button");
	
	
	public void doOpenMyEhrPage() {
		FrameWork.Click(driver, welcome);
		FrameWork.Click(driver, myehr);
		Log.info("opening My Ehr page");
	}

	public void doVerifyMyEhrPage() {
//		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading,MyEhrData.title));
		Assert.assertEquals(driver.getTitle(), MyEhrData.title);
	
	}
	
	public void doAddMyEhrPage(String EhrType){
		
		if(EhrType == "Bills"){
		FrameWork.Click(driver, btnAddNewEHR);
		//Thread.sleep(1000);
		
		FrameWork.Click(driver, btnAddTag);
		FrameWork.Type(driver, txtdateehr,MyEhrData.txtdateehr);
		FrameWork.Click(driver, btnBills);
		
		FrameWork.Type(driver, txtdoctor, MyEhrData.txtdoctor);
		
		FrameWork.Type(driver, txthospital, MyEhrData.txthospital);
		
		FrameWork.Type(driver, txtdiease, MyEhrData.txtdiease);
		
		//FrameWork.Click(driver, btnsaveehr);
		FrameWork.Click(driver, btnAddNewEHR);	
		}
		
		if(EhrType == "Reports"){
			FrameWork.Click(driver, btnAddNewEHR);
		
			FrameWork.Click(driver, btnAddTag);
			FrameWork.Type(driver, txtdateehr,MyEhrData.txtdateehr);
			FrameWork.Click(driver, btnReports);
			
			FrameWork.Type(driver, txtdoctor, MyEhrData.txtdoctor);
			
			FrameWork.Type(driver, txthospital, MyEhrData.txthospital);
		
			FrameWork.Type(driver, txtdiease, MyEhrData.txtdiease);
			
			FrameWork.Type(driver, txtreporttype, MyEhrData.txtreporttype);
			
			//FrameWork.Click(driver, btnsaveehr);
			FrameWork.Click(driver, btnAddNewEHR);	
			}
		
	}
	
	public void AddMyEhrBlankFields(String EhrType){
		
		FrameWork.Click(driver, btnAddNewEHR);
		
		FrameWork.Click(driver, btnAddTag);
		driver.findElement(txtdateehr).clear();
		driver.findElement(txtdoctor).clear();
		driver.findElement(txthospital).clear();
		driver.findElement(txtdiease).clear();
		FrameWork.Click(driver, btnsaveehr);
		Assert.assertTrue(FrameWork.
				verifyErrorMessage(driver, By.xpath("html/body/div[5]/div/div/div[1]/div"),
						"Please upload atleast one EHR file."), "EHR file has uploaded");
		FrameWork.Click(driver, btnOK);
	}
		
}