package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.mycol.automation.factorydata.notInMyColfactory;
import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;

public class notInMyCol {
	private By name  = By.id("name");
	private By address1 = By.id("address1");
	private By address2 = By.id("address2");
	private By city = By.id("cityList");
	private By localityList = By.id("localityList");
	private By pincode = By.id("pincode");
	private By website_link = By.id("website_link");
	private By email = By.id("email");
	private By telephone_0 = By.id("telephone_0");
	private By fax_0 = By.id("fax_0");
	private By ambulance_0 = By.id("ambulance_0");
	private By hospital_image_file = By.id("hospital_image_file");
	private By hospital_image_section = By.id("section"); 
	private By hospital_lat = By.id("hospital_lat");
	private By hospital_lng = By.id("hospital_lng");
	private By scuccessmsg = By.xpath(".//*[@id='content']/div[1]");
	private By btnsubmit = By.xpath(".//*[@id='addhospital']/div[18]/div/button");
	String chkdepartment = "department_";
	private By link_hospital = By.linkText("Hospital Not on myCOL");
	
	WebDriver driver;
	
	public notInMyCol(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting SignInPage ... ");
	}
	
	public void doOpenHospital()
	{
		FrameWork.Click(driver,link_hospital);
	}
	public void doAddHospital() throws InterruptedException
	{
		doOpenHospital();
		FrameWork.Type(driver, name,notInMyColfactory.notInMyColData.name);
		FrameWork.Type(driver, address1,notInMyColfactory.notInMyColData.address1);
		FrameWork.Type(driver, address2,notInMyColfactory.notInMyColData.address2);
		FrameWork.SelectByVisibleText(driver, city,notInMyColfactory.notInMyColData.city);
	//	FrameWork.SelectByVisibleText(driver, localityList,notInMyColfactory.notInMyColData.localityList);
		FrameWork.Type(driver, pincode,notInMyColfactory.notInMyColData.pincode);
		FrameWork.Type(driver, website_link,notInMyColfactory.notInMyColData.website_link);
		FrameWork.Type(driver, email,notInMyColfactory.notInMyColData.email);
		FrameWork.Type(driver, telephone_0,notInMyColfactory.notInMyColData.telephone_0);
		FrameWork.Type(driver, fax_0,notInMyColfactory.notInMyColData.fax_0);
		FrameWork.Type(driver, ambulance_0,notInMyColfactory.notInMyColData.ambulance_0);
		//FrameWork.verifyGroupInputEle(driver, chkdepartment,1,25);
		FrameWork.clickOnGroupInputEle(driver, chkdepartment,1,25);
		FrameWork.SelectByVisibleText(driver, hospital_image_section,notInMyColfactory.notInMyColData.hospital_image_section);
		FrameWork.Type(driver, hospital_image_file,notInMyColfactory.notInMyColData.hospital_image_file);
		FrameWork.Type(driver, hospital_lat,notInMyColfactory.notInMyColData.hospital_lat);
		FrameWork.Type(driver, hospital_lng,notInMyColfactory.notInMyColData.hospital_lng);
		Thread.sleep(6000);
		//System.out.println("<script type='text/javascript'> var y=window.prompt('please enter captcha code')</script>");
		FrameWork.Click(driver, btnsubmit);
		FrameWork.verifySuccessMessage(driver, scuccessmsg, notInMyColfactory.notInMyColData.successMessage);
	}
	
	public void doBlankAddHospital()
	{
		FrameWork.Click(driver, btnsubmit);
	}

}
