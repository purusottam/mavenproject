package com.mycol.automation.pagefactory;

import java.awt.Frame;

import org.junit.runners.model.FrameworkField;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.Hospital;

public class AdminAddHospitalMap {

	protected WebDriver driver;

	private By linkpage = By.xpath(".//*[@id='invoke-googlemap']");
	private By pageheading = By.xpath(".//*[@id='content']/h1");
	private By latitude = By.id("Hospital_lat");
	private By longitude = By.id("Hospital_lng");
	private By btngetmelocation = By
			.xpath("//*[@id='hospital-form-9']/div[2]/input[2]");
	private By btnsubmit = By.xpath(".//*[@id='hospital-form-9']/div[3]/input");
	private By successmsg = By.xpath("//*[@id='msg-hospital-form-9']");

	public AdminAddHospitalMap(WebDriver driver) {
		this.driver = driver;
	}

	public void openAddHospitalMapPage() {

		FrameWork.Click(driver, linkpage);
	}

	public void verifyAddHospitalMapPageHeading() {
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading,
				Hospital.googlemap.pageheading));
	}

	public void doAddHospitalMap() {
		Log.startTestCase("doAddHospitalMap");
		FrameWork.Type(driver, latitude, Hospital.googlemap.latitude);
		FrameWork.Type(driver, longitude, Hospital.googlemap.longitude);
		FrameWork.Click(driver, btngetmelocation);
		FrameWork.Click(driver, btnsubmit);
		Log.endTestCase("doAddHospitalMap");
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, successmsg,
				Hospital.googlemap.successmsgText));

	}

	public void verifyHospitalMap() {
		Log.startTestCase("verifyHospitalMap");
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, latitude,
				Hospital.googlemap.latitude));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, longitude,
				Hospital.googlemap.longitude));
		Log.endTestCase("verifyHospitalMap");
	}

}
