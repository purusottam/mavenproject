package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.AdminMasterDepartmentManagerData;

public class AdminMasterDepartmentManager {
	
	protected WebDriver driver;

	private By master = By.linkText("Masters");
	private By departmentmanagerlink = By.linkText("Department Manager");
	private By btnAddDepatment = By.linkText("Add Department");
	private By department = By.id("Department_department");
	private By status = By.id("Department_status");
	private By successnotification = By.xpath(".//*[@id='content']/ul/li/div");
	private By create = By.name("yt0");
	private By addDepartmentpageheading = By.xpath(".//*[@id='content']/h1");
	protected String update_url = "http://admin.staging.colhealth.org/index.php/departments/update/26";
	private By departmentblankfielderrormessage = By.xpath(".//*[@id='department-form']/div[2]/div");

	
	public AdminMasterDepartmentManager(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting AdminMasterDepartmentManager ....");
	}
	
	public Boolean doVerifyDepartmentManagerPage() {

		Log.startTestCase("doVerifyDepartmentManagerPage");
		if (driver.getTitle().matches(AdminMasterDepartmentManagerData.title)) {
			Log.info("Pass doVerifyDepartmentManagerPage");
			return true;
		} else {
			Log.error("Error in doVerifyDepartmentManagerPage");
			return false;
		}
	}
	
	public void doOpenDepartmentManagerPage() {
		Log.info("Opening doOpenDepartmentManagerPage");
		FrameWork.Click(driver, master);
		FrameWork.Click(driver, departmentmanagerlink);
	}

	public void doOpenAddDepartmentPage() {
		Log.info("Opening doOpenAddDepartmentPage");
		FrameWork.Click(driver, btnAddDepatment);
	}
	
	public Boolean doVerifyAddDepartmentPage() {
		if (driver.getTitle().matches(AdminMasterDepartmentManagerData.addDepartmenttitle)
				&& FrameWork.GetText(driver, addDepartmentpageheading).matches(
						AdminMasterDepartmentManagerData.addDepartmentpageheadingText)) {
			Log.info("Pass doVerifyAddDepartmentPage");
			return true;
		} else {
			Log.error("Error in doVerifyAddDepartmentPage");
			return false;
		}
	}

	public void doAddDepartment() throws InterruptedException {
		Log.info("Adding doAddDepartment");
		
		FrameWork.Type(driver, department, AdminMasterDepartmentManagerData.departmentText);
		
		FrameWork.SelectByVisibleText(driver, status, AdminMasterDepartmentManagerData.statusText);
		
		FrameWork.Click(driver, create);
		Thread.sleep(3000);
		String successNotification = "Department "
				+ AdminMasterDepartmentManagerData.departmentText + " added Successfully";
		
		FrameWork.verifySuccessMessage(driver, successnotification,	successNotification);

	}
	
	public void doUpdateDepartment() throws InterruptedException {
		driver.get(update_url);
		doAddDepartment();
		driver.get(update_url);
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, department,
				AdminMasterDepartmentManagerData.departmentText));
		Assert.assertTrue(FrameWork.verifyDropDownData(driver, status,
				AdminMasterDepartmentManagerData.statusText));
	}

	public void doAddDepartmentBlankField() throws InterruptedException {
		doOpenDepartmentManagerPage();
		doOpenAddDepartmentPage();
		driver.findElement(department).clear();
		FrameWork.Click(driver, create);
		
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver, departmentblankfielderrormessage,
					AdminMasterDepartmentManagerData.departmentblankfielderrormessageText));
	}

}
