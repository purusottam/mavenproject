package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.*;

public class CreateAccountPage {

	private WebDriver driver;
	private By poppup = By.xpath(".//*[@id='myModal']/div/div/div[1]/button");
	private By popupcity = By.id("city");
	private By popupbtn = By.id("saveCity");

	public By RegisterMobileOrEmail = By.name("email");
	public By RegisterPassword = By.name("password");
	private By RegsiterRePassword = By.name("confirmPassword");
	private By RegisterErrorMobileOrEmail = By
			.xpath(".//*[@id='signup']/div[1]/div/label");
	private By RegisterErrorPassword = By
			.xpath(".//*[@id='signup']/div[2]/div/label");
	private By RegsiterErrorRePassword = By
			.xpath(".//*[@id='signup']/div[3]/div/label");
	private By RegisterButton = By
			.xpath(".//*[@id='signup']/div[4]/div[2]/button");
	private By RegisterMessage = By.xpath(".//*[@id='content']/div[1]");
	private By RegisterPageHeading = By.xpath(".//*[@id='signup']/h2");
	private By RegisterLink = By.linkText("Sign Up");
	private By RegisterMobileSuccess = By.xpath(".//*[@id='content']/div");
	

	public CreateAccountPage(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting CreateAccountPage");
		popupCityChoose();
	}

	public String getPageTitle() {
		String title = driver.getTitle();
		return title;
	}

	public boolean verifyPageTitle() {
		String pageTitle = "";
		return getPageTitle().contains(pageTitle);
	}

	public void popupCityChoose() {
		FrameWork.SelectByVisibleText(driver, popupcity,
				Createaccountpage.CreateaccountpageData.popupcity);
		FrameWork.Click(driver, popupbtn);
	}

	public boolean verifyPageHeading() {
		if (driver.findElement(RegisterPageHeading).getText()
				.matches(Createaccountpage.CreateaccountpageData.PageHeading)) {
			Log.info("Pass verifyPageHeading");
			return true;
		} else
			Log.error("Error verifyPageHeading");
		return false;
	}

	public void openRegisterPage() {
		Log.startTestCase("Opening register page");

		driver.findElement(RegisterLink).click();
	}

	public void createWrongAccountByAlphabat() {

		driver.findElement(RegisterMobileOrEmail).sendKeys("sdfddf");
		driver.findElement(RegisterPassword).sendKeys(
				Createaccountpage.CreateaccountpageData.password);
		driver.findElement(RegsiterRePassword).sendKeys(
				Createaccountpage.CreateaccountpageData.repassword);
		driver.findElement(RegisterButton).click();
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver,
				RegisterErrorMobileOrEmail,
				Createaccountpage.CreateaccountpageData.nameErrorForVaild));

	}

	public void createAccountByEmail() throws InterruptedException {

		Log.info("Create ing Account by Email");
		FrameWork.Type(driver, RegisterMobileOrEmail,
				Createaccountpage.CreateaccountpageData.name);
		FrameWork.Type(driver, RegisterPassword,
				Createaccountpage.CreateaccountpageData.password);
		FrameWork.Type(driver, RegsiterRePassword,
				Createaccountpage.CreateaccountpageData.repassword);
		driver.findElement(RegisterButton).click();

	}

	public void createAccountByMobile() {

		if (driver.findElement(RegisterMobileOrEmail).isDisplayed()) {
			Log.info("creating Account By Mobile ");
			driver.findElement(RegisterMobileOrEmail).sendKeys(
					Createaccountpage.CreateaccountpageData.mobile);
			driver.findElement(RegisterPassword).sendKeys(
					Createaccountpage.CreateaccountpageData.password);
			driver.findElement(RegsiterRePassword).sendKeys(
					Createaccountpage.CreateaccountpageData.repassword);
			driver.findElement(RegisterButton).click();

		}

	}

	public boolean verifyMobileRegisterAccount() {
		WebDriverWait wait1 = new WebDriverWait(driver, 28);
		WebElement w1 = driver.findElement(RegisterMobileSuccess);
		wait1.until(ExpectedConditions.textToBePresentInElement(w1,
				Createaccountpage.CreateaccountpageData.mobilesuccess));
		if (driver.findElement(RegisterMobileSuccess).getText()
				.matches(Createaccountpage.CreateaccountpageData.mobilesuccess)) {
			Log.info("Pass verifyMobileRegisterAccount ");
			return true;
		} else {
			Log.error("Error verifyMobileRegisterAccount");
			return false;
		}
	}

	public void verifyEmailRegisterAccount() {

		FrameWork.verifySuccessMessage(driver, RegisterMessage,
				Createaccountpage.CreateaccountpageData.success);

	}

	public boolean blankMobileOrEmail() {

		driver.findElement(RegisterPassword).sendKeys(
				Createaccountpage.CreateaccountpageData.password);
		driver.findElement(RegsiterRePassword).sendKeys(
				Createaccountpage.CreateaccountpageData.repassword);
		driver.findElement(RegisterButton).click();
		if (driver.findElement(RegisterErrorMobileOrEmail).getText()
				.matches(Createaccountpage.CreateaccountpageData.nameError)) {
			Log.info("Pass blankMobileOrEmail");
			return true;
		} else {
			Log.error("Error blankMobileOrEmail");
			return false;
		}

	}

	public boolean blankPassword() {
		driver.findElement(RegisterMobileOrEmail).sendKeys(
				Createaccountpage.CreateaccountpageData.name);
		// driver.findElement(RegisterPassword).sendKeys(password);
		driver.findElement(RegsiterRePassword).sendKeys(
				Createaccountpage.CreateaccountpageData.repassword);
		driver.findElement(RegisterButton).click();

		if (driver.findElement(RegisterErrorPassword).getText()
				.matches(Createaccountpage.CreateaccountpageData.passwordError)) {
			Log.info("Pass blankPassword");
			return true;
		} else {
			Log.error("Error blankPassword");
			return false;
		}
	}

	public boolean blankRePassword() {

		driver.findElement(RegisterMobileOrEmail).sendKeys(
				Createaccountpage.CreateaccountpageData.name);
		// driver.findElement(RegisterPassword).sendKeys(password);
		driver.findElement(RegsiterRePassword).sendKeys(
				Createaccountpage.CreateaccountpageData.repassword);
		driver.findElement(RegisterButton).click();
		if (driver
				.findElement(RegsiterErrorRePassword)
				.getText()
				.matches(
						Createaccountpage.CreateaccountpageData.repasswordError)) {
			Log.info("Pass blankRePassword");
			return true;
		} else {
			Log.info("Error blankRePassword");
			return false;
		}
	}

	public void clearRegisterField() {
		driver.findElement(RegisterMobileOrEmail).clear();
		driver.findElement(RegisterPassword).clear();
		driver.findElement(RegsiterRePassword).clear();
	}

	public boolean verifyemailormobilePlaceHolderText() {
		if (driver
				.findElement(RegisterMobileOrEmail)
				.getAttribute("placeholder")
				.matches(
						Createaccountpage.CreateaccountpageData.nameplaceholder)) {
			Log.info("Pass verifyemailormobilePlaceHolderText");
			return true;
		} else {
			Log.error("Error in verifyemailormobilePlaceHolderText");
			return false;
		}
	}

	public boolean verifyPasswordPlaceHolderText() {
		if (driver
				.findElement(RegisterPassword)
				.getAttribute("placeholder")
				.matches(
						Createaccountpage.CreateaccountpageData.passwordplaceholder)) {
			Log.info("Pass verifyPasswordPlaceHolderText");
			return true;
		} else {
			Log.error("Error in verifyPasswordPlaceHolderText");
			return false;
		}
	}

	public boolean verifyRePasswordPlaceHolderText() {
		if (driver
				.findElement(RegsiterRePassword)
				.getAttribute("placeholder")
				.matches(
						Createaccountpage.CreateaccountpageData.repasswordplaceholder)) {
			Log.info("Pass verifyRePasswordPlaceHolderText");
			return true;
		} else {
			Log.error("Error in verifyRePasswordPlaceHolderText");
			return false;
		}
	}
	
	public void doVerifyExistingEmailid()
	{
		driver.findElement(RegisterMobileOrEmail).sendKeys(Createaccountpage.CreateaccountpageData.existing_email);
		driver.findElement(RegisterPassword).sendKeys(
				Createaccountpage.CreateaccountpageData.password);
		driver.findElement(RegsiterRePassword).sendKeys(
				Createaccountpage.CreateaccountpageData.repassword);
		driver.findElement(RegisterButton).click();
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver,
				RegisterMessage,
				Createaccountpage.CreateaccountpageData.existing_emailText));

	}
	
	public void doVerifyExistingMobile()
	{
		driver.findElement(RegisterMobileOrEmail).sendKeys(Createaccountpage.CreateaccountpageData.existing_mobile);
		driver.findElement(RegisterPassword).sendKeys(
				Createaccountpage.CreateaccountpageData.password);
		driver.findElement(RegsiterRePassword).sendKeys(
				Createaccountpage.CreateaccountpageData.repassword);
		driver.findElement(RegisterButton).click();
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver,
				RegisterMessage,
				Createaccountpage.CreateaccountpageData.existingmobileText));

	}
	
	
}