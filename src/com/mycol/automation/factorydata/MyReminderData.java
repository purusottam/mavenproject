package com.mycol.automation.factorydata;

public class MyReminderData {

	public static String title = "myCol - My Reminder";
	public static String txtTitle = "Medicine";
	public static String txtDrugName = "Voveran";
	public static String drpdwnReminderTimesText1 = "Once a day";
	public static String drpdwnReminderTimesText2 = "Twice a day";
	public static String drpdwnReminderTimesText3 = "3 Times a day";
	public static String drpdwnReminderTimesText4 = "4 Times a day";
	public static String txtTime1 = "1:00 PM";
	public static String txtTime2 = "9:00 PM";
	public static String txtTime3 = "5:00 PM";
	public static String txtTime4 = "9:00 AM";
	public static String txtQuantity1 = "1";
	public static String txtQuantity2 = "1";
	public static String txtQuantity3 = "1";
	public static String txtQuantity4 = "1";
	public static String txtStartDate = "01-07-2015";
	public static String txtDaysNumber = "5";
	public static String drpdwnAlarmText = "Chime till accepted";
	public static String appointmentTitleText = "Doctor's Appointment";
	public static String appointmentDateText = "07-07-2015";
	public static String appointmentTimeText = "8:00 PM";
	public static String appointmentHospitalNameText = "A One Hospital Pvt. Ltd.";
	public static String appointmentDoctorNameText = "Dr Mahesh Kapoor";
	public static String appointmentAlarmText = "Chime till accepted";
	public static String drpdwnAlarmDefaultText = "select alarm option";
	public static String titleBlankfieldErrormessageText = "This field is required.";
	public static String drugNameBlankfieldErrormessageText = "This field is required.";
	public static String alarmBlankfieldErrormessageText = "This field is required.";
	public static String caretakerBlankfieldErrormessageText = "This field is required.";
	public static String dateBlankfieldErrormessageText = "This field is required.";
	public static String timeBlankfieldErrormessageText = "This field is required.";
	public static String hospitalNameBlankfieldErrormessageText = "This field is required.";
	public static String doctorNameBlankfieldErrormessageText = "This field is required.";
	public static String reminderSuccessMsgText = "Reminder has been saved successfully.";
	
}
