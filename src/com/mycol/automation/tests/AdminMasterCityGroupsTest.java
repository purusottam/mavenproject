package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.AdminLogin;
import com.mycol.automation.pagefactory.AdminMasterCityGroups;

public class AdminMasterCityGroupsTest extends TestBaseSetup {
	private WebDriver driver;
	
	AdminLogin signin;
	AdminMasterCityGroups  citygrp;
							
	@BeforeClass
	public void setup() {
		driver = getDriver();
		signin = new AdminLogin(driver);
		citygrp = new AdminMasterCityGroups(driver);
			}

	@Test(priority=0)
	public void doVerifyCityGroupManagePageTest()
	{
		signin.doLogin();
		citygrp.doOpenManageCityGroupPage();
		Assert.assertTrue(citygrp.doVerifyCityGroupManagePage());
	}
	
	@Test(priority=1)
	public void doVerifyAddGroupPageTest() throws InterruptedException
	{
		citygrp.doOpenAddCityGroupPage();
		citygrp.doAddCityGroup();
	}
	
	@Test(priority=2,enabled=false)
	public void doUpdateCityGroupTest() throws InterruptedException
	{
		citygrp.doUpdateCityGroup();
	}
	
	@Test(priority=3)
	public void doBlankAllFieldTest() throws InterruptedException
	{
		citygrp.doBlankAllField();
	}
	
	
}
