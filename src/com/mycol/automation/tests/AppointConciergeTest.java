package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.AppointConcierge;
import com.mycol.automation.pagefactory.SignInPage;

public class AppointConciergeTest extends TestBaseSetup {

	private WebDriver driver;
	private AppointConcierge appcon;
	private SignInPage signInPage;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		signInPage = new SignInPage(driver);
		appcon = new AppointConcierge(driver);

	}

	@Test(priority = 0)
	public void verifyAppointConciergePopupTest() throws InterruptedException {
		signInPage.doLogin();
		appcon.openHospitalPage();
		appcon.openAppointConciergePopup();
		//appcon.verifyAppointConciergePopup();
	}

	@Test(priority = 1)
	public void doAddAppointConciergeTest() {
		appcon.doAddAppointConcierge();

	}

	@Test(priority = 2)
	public void vefiyMandatoryFieldsTest() {

		appcon.vefiyMandatoryFields();

	}

}
