package com.mycol.automation.tests;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.Library;
import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.notInMyCol;
import com.mycol.automation.pagefactory.SignInPage;

public class notInMyColfactoryTest extends TestBaseSetup {
	private WebDriver driver;

	private SignInPage signInPage;
	private notInMyCol notInMyCol;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		notInMyCol = new notInMyCol(driver);
		signInPage = new SignInPage(driver);
	}
	
	@Test
	public void doAddHospital() throws InterruptedException
	{
		//signInPage.doLogin();
		Library.popupCityChoose(driver);
		notInMyCol.doAddHospital();
		
	}
	
}
