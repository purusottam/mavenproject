package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.MyEhr;
import com.mycol.automation.pagefactory.SignInPage;

public class MyEhrTest extends TestBaseSetup {
	private WebDriver driver;

	private SignInPage signInPage;
	private MyEhr myehr;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		myehr = new MyEhr(driver);
		signInPage = new SignInPage(driver);

	}

	@Test(priority = 0, enabled = true)
	public void doOpenMyEhrPageTest() throws InterruptedException {
		signInPage.doLogin();
		myehr.doOpenMyEhrPage();
	}

	@Test(priority = 1, enabled = true)
	public void doVerifyPageHeadingTest() {
		myehr.doVerifyMyEhrPage();
	}
	
	@Test(priority = 2, enabled = true)
	public void AddMyEhrPageTest() throws InterruptedException {
		myehr.doAddMyEhrPage("Bills");
		myehr.doAddMyEhrPage("Reports");
	//We can run the script for different EHR Types(Bills, Reports)
	}
	
}
