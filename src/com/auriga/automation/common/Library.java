package com.auriga.automation.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mycol.automation.factorydata.Createaccountpage;
public class Library
{
	private static By popupcity = By.id("city");
	private static By popupbtn = By.id("saveCity");
	private static By lblpopup = By.xpath(".//*[@id='myModalLabel']");
	private static By pagebody = By.id("myModal");
	private static String lbltxt = "Select your City";
	private static By selectKMR = By.xpath(".//*[@id='city']/option[2]");
	
	public static void popupCityChoose(WebDriver driver) throws InterruptedException {
	//	Thread.sleep(5000);
		
	//	FrameWork.WaitForPageToLoad(driver, 3);
	//	FrameWork.Click(driver, pagebody);
	//	FrameWork.WaitForDropDown(driver,popupcity);
		FrameWork.Click(driver, popupcity);
		FrameWork.Click(driver, selectKMR);
	//	FrameWork.SelectByVisibleText(driver, popupcity, Createaccountpage.CreateaccountpageData.popupcity);
		
		//new Select(driver.findElement(popupcity)).selectByVisibleText(Createaccountpage.CreateaccountpageData.popupcity);
		Log.info("Selected City in Popup "+FrameWork.GetText(driver, popupcity));
    	Thread.sleep(5000);
		FrameWork.Click(driver, popupbtn);
		
		
	}

}
